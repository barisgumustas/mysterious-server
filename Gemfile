ruby '2.3.0'
source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5.1'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use SCSS for stylesheets
# gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
# gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
# gem 'sdoc', '~> 0.4.0', group: :doc

gem 'devise', '~> 3.5.5'
gem 'pundit', '~> 1.1.0'
gem 'active_model_serializers', '~> 0.8.0'

gem 'puma'
gem 'awesome_print'

gem 'kaminari'
gem 'oj'
gem 'oj_mimic_json'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  gem 'rubocop', require: false
  gem 'guard'
  gem 'guard-minitest'
  gem 'guard-rubocop'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  gem 'webmock'
  gem 'database_cleaner'
  gem 'minitest-reporters'
  gem 'minitest-stub_any_instance'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'simplecov', require: false

  gem 'shoulda', '~> 3.5'
  gem 'shoulda-matchers', '~> 2.0'
end
