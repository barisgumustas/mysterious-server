FactoryGirl.define do
  factory :task do
    sequence(:description) { |n| "Task #{n}" }
    association :user
    association :list
  end
end
