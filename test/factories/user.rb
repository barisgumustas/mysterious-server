FactoryGirl.define do
  factory :user do
    sequence(:email)  { |n| "user-#{n}@test.com" }
    password          { SecureRandom.hex(8) }
    role              'user'
  end

  factory :admin_user, parent: :user do
    role 'admin'
  end

  factory :guest_user, parent: :user do
    role 'guest'
  end
end
