FactoryGirl.define do
  factory :list do
    sequence(:title) { |n| "List #{n}" }
    association :user
  end
end
