require 'test_helper'

class ListTest < ActiveSupport::TestCase
  should belong_to(:user)
  should have_many(:tasks)

  should validate_presence_of(:title)
  should validate_presence_of(:user)
end
