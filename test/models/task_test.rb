require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  should belong_to(:user)
  should belong_to(:list)

  should validate_presence_of(:description)
  should validate_presence_of(:list)
  should validate_presence_of(:user)
end
