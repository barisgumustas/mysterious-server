require 'test_helper'
require 'user' # https://github.com/colszowka/simplecov/issues/60#issuecomment-3117707

class UserTest < ActiveSupport::TestCase
  should have_many(:lists)
  should have_many(:tasks)

  should validate_inclusion_of(:role).in_array(AVAILABLE_USER_ROLES)
  should validate_presence_of(:role)

  test 'admin?' do
    user = FactoryGirl.build(:admin_user)
    assert user.admin?
  end

  test 'user?' do
    user = FactoryGirl.build(:user)
    assert user.user?
  end

  test 'guest?' do
    user = FactoryGirl.build(:guest_user)
    assert user.guest?
  end

  test 'owns?' do
    user = FactoryGirl.build(:user)

    owned_record = OpenStruct.new(user: user)
    owned_record_with_another_accessor = OpenStruct.new(owner: user)
    non_owned_record = OpenStruct.new(user: FactoryGirl.build(:user))

    assert user.owns?(owned_record)
    assert user.owns?(owned_record_with_another_accessor, :owner)

    assert !user.owns?(non_owned_record)
  end
end
