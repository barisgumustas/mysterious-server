require 'test_helper'

class ApplicationTestController < ApplicationController
  def test
    render plain: 'OK'
  end

  def policy_error
    raise Pundit::NotAuthorizedError, 'policy error message'
  end

  def not_found
    raise ActiveRecord::RecordNotFound
  end
end

class ApplicationControllerTest < ActionController::TestCase
  tests ApplicationTestController

  def setup
    @routes.draw do
      get 'application_test'          => 'application_test#test'
      get 'application_policy_error'  => 'application_test#policy_error'
      get 'application_not_found'     => 'application_test#not_found'
    end
  end

  def teardown
    Rails.application.reload_routes!
  end

  test 'should return 401 without an authentication header' do
    get :test
    assert_response :unauthorized
  end

  test 'should return 401 with an invalid authentication header' do
    add_http_authentication_header('invalid_username', 'invalid_password')

    get :test
    assert_response :unauthorized
  end

  test 'should return render OK with a valid authentication header' do
    sign_in

    get :test
    assert_response :success
    assert_equal response.body, 'OK'
  end

  test 'should respond Pundit::NotAuthorizedError with an error json' do
    sign_in

    get :policy_error
    assert_response :unauthorized
    assert_includes response_json['errors'], 'policy error message'
  end

  test 'should respond ActiveRecord::RecordNotFound with an error json' do
    sign_in

    get :not_found
    assert_response 404
    assert_includes response_json['errors'], 'Requested resource not found.'
  end
end
