require 'test_helper'

class TasksControllerTest < ActionController::TestCase
  module ReadOnlyActionTests
    extend ActiveSupport::Concern

    included do
      test 'index action' do
        get :index
        assert_response :success
      end

      test 'show action with a valid id' do
        task = FactoryGirl.create(:task, user: @user)

        get :show, id: task.id
        assert_response :success
      end

      test 'show action with an invalid id' do
        get :show, id: 'invalid_id'
        assert_response :not_found
      end
    end
  end

  module AuthorizedCreateActionTests
    extend ActiveSupport::Concern

    included do
      test 'create action with valid params' do
        assert_difference('Task.count', 1) do
          list = FactoryGirl.create(:list)
          post :create, task: FactoryGirl.attributes_for(:task, user: nil, list_id: list.id)
          assert_response :success
        end
      end

      test 'create action with invalid params' do
        assert_difference('Task.count', 0) do
          post :create
          assert_response 422
          assert_includes response_json['errors'], "Description can't be blank"
          assert_includes response_json['errors'], "List can't be blank"
        end
      end
    end
  end

  module AuthorizedUpdateActionAndDestroyTests
    extend ActiveSupport::Concern

    included do
      test 'update action on an owned record with valid params' do
        task = FactoryGirl.create(:task, description: 'Sample Task', user: @user)

        put :update, id: task.id, task: { description: 'Modified Task' }
        assert_response :success
        assert_equal 'Modified Task', task.reload.description
      end

      test 'update action on an owned record with invalid params' do
        task = FactoryGirl.create(:task, description: 'Sample Task', user: @user)

        put :update, id: task.id, task: { description: nil }
        assert_response 422
        assert_includes response_json['errors'], "Description can't be blank"
      end

      test 'destroy action on an owned record with valid params' do
        task = FactoryGirl.create(:task, user: @user)

        assert_difference('Task.count', -1) do
          delete :destroy, id: task.id
          assert_response :success
        end
      end
    end
  end

  class AsUserTest < ActionController::TestCase
    include ReadOnlyActionTests
    include AuthorizedCreateActionTests
    include AuthorizedUpdateActionAndDestroyTests

    def setup
      @user = sign_in
    end

    test 'update action on a not-owned record' do
      task = FactoryGirl.create(:task, description: 'Sample Task')

      put :update, id: task.id, task: { description: nil }
      assert_response :unauthorized
    end
  end

  class AsGuestTest < ActionController::TestCase
    include ReadOnlyActionTests

    def setup
      @user = sign_in(FactoryGirl.create(:guest_user))
    end

    test 'create action' do
      post :create, task: { description: 'Sample Task' }
      assert_response :unauthorized
    end

    test 'update action' do
      task = FactoryGirl.create(:task, description: 'Sample Task')

      put :update, id: task.id, task: { description: nil }
      assert_response :unauthorized
    end
  end

  class AsAdminTest < ActionController::TestCase
    include ReadOnlyActionTests
    include AuthorizedCreateActionTests
    include AuthorizedUpdateActionAndDestroyTests

    def setup
      @user = sign_in(FactoryGirl.create(:admin_user))
    end

    test 'update action on a not-owned record' do
      task = FactoryGirl.create(:task, description: 'Sample Task')

      put :update, id: task.id, task: { description: 'Modified Task' }
      assert_response :success
      assert_equal 'Modified Task', task.reload.description
    end
  end
end
