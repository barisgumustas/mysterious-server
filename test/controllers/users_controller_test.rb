require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user = sign_in
  end

  test 'show action with me as id param' do
    get :show, id: 'me'
    assert_redirected_to controller: :users, action: :show, id: @user.id
  end

  test 'show action with a valid id param' do
    get :show, id: @user.id
    assert_response :success
  end

  test 'show action with an invalid id param' do
    get :show, id: 'INVALID_ID'
    assert_response :not_found
  end
end
