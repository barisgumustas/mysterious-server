require 'test_helper'

class ListsControllerTest < ActionController::TestCase
  module ReadOnlyActionTests
    extend ActiveSupport::Concern

    included do
      test 'index action' do
        get :index
        assert_response :success
      end

      test 'show action with a valid id' do
        list = FactoryGirl.create(:list, user: @user)

        get :show, id: list.id
        assert_response :success
      end

      test 'show action with an invalid id' do
        get :show, id: 'invalid_id'
        assert_response :not_found
      end
    end
  end

  module AuthorizedCreateActionTests
    extend ActiveSupport::Concern

    included do
      test 'create action with valid params' do
        assert_difference('List.count', 1) do
          post :create, list: FactoryGirl.attributes_for(:list, user: nil)
          assert_response :success
        end
      end

      test 'create action with invalid params' do
        assert_difference('List.count', 0) do
          post :create
          assert_response 422
          assert_includes response_json['errors'], "Title can't be blank"
        end
      end
    end
  end

  module AuthorizedUpdateActionAndDestroyTests
    extend ActiveSupport::Concern

    included do
      test 'update action on an owned record with valid params' do
        list = FactoryGirl.create(:list, title: 'Sample List', user: @user)

        put :update, id: list.id, list: { title: 'Modified List' }
        assert_response :success
        assert_equal 'Modified List', list.reload.title
      end

      test 'update action on an owned record with invalid params' do
        list = FactoryGirl.create(:list, title: 'Sample List', user: @user)

        put :update, id: list.id, list: { title: nil }
        assert_response 422
        assert_includes response_json['errors'], "Title can't be blank"
      end

      test 'destroy action on an owned record with valid params' do
        list = FactoryGirl.create(:list, user: @user)
        FactoryGirl.create(:task, list: list, user: @user)

        assert_difference('List.count', -1) do
          assert_difference('Task.count', -1) do
            delete :destroy, id: list.id
            assert_response :success
          end
        end
      end
    end
  end

  class AsUserTest < ActionController::TestCase
    include ReadOnlyActionTests
    include AuthorizedCreateActionTests
    include AuthorizedUpdateActionAndDestroyTests

    def setup
      @user = sign_in
    end

    test 'update action on a not-owned record' do
      list = FactoryGirl.create(:list, title: 'Sample List')

      put :update, id: list.id, list: { title: nil }
      assert_response :unauthorized
    end

    test 'destroy action on an owned record with a task from another user' do
      list = FactoryGirl.create(:list, title: 'Sample List', user: @user)
      FactoryGirl.create(:task, list: list)

      delete :destroy, id: list.id
      assert_response :unauthorized
      explanation = "Can't destroy the list while it contains 1 tasks owned by other users."
      assert_includes response_json['errors'], explanation
    end
  end

  class AsGuestTest < ActionController::TestCase
    include ReadOnlyActionTests

    def setup
      @user = sign_in(FactoryGirl.create(:guest_user))
    end

    test 'create action' do
      post :create, list: { title: 'Sample List' }
      assert_response :unauthorized
    end

    test 'update action' do
      list = FactoryGirl.create(:list, title: 'Sample List')

      put :update, id: list.id, list: { title: nil }
      assert_response :unauthorized
    end
  end

  class AsAdminTest < ActionController::TestCase
    include ReadOnlyActionTests
    include AuthorizedCreateActionTests
    include AuthorizedUpdateActionAndDestroyTests

    def setup
      @user = sign_in(FactoryGirl.create(:admin_user))
    end

    test 'update action on a not-owned record' do
      list = FactoryGirl.create(:list, title: 'Sample List')

      put :update, id: list.id, list: { title: 'Modified List' }
      assert_response :success
      assert_equal 'Modified List', list.reload.title
    end

    test 'destroy action on a not-owned record' do
      list = FactoryGirl.create(:list)
      FactoryGirl.create(:task, list: list)

      assert_difference('List.count', -1) do
        assert_difference('Task.count', -1) do
          delete :destroy, id: list.id
          assert_response :success
        end
      end
    end

    test 'destroy action on an owned record with a task from another user' do
      list = FactoryGirl.create(:list, title: 'Sample List', user: @user)
      FactoryGirl.create(:task, list: list)

      assert_difference('List.count', -1) do
        assert_difference('Task.count', -1) do
          delete :destroy, id: list.id
          assert_response :success
        end
      end
    end
  end
end
