if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start('rails')
end

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

MiniTest::Reporters.use! MiniTest::Reporters::DefaultReporter.new(color: true)
DatabaseCleaner.strategy = :transaction

class ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  # fixtures :all

  def sign_in(user = nil)
    user ||= FactoryGirl.create(:user)
    add_http_authentication_header(user.email, user.password) && user
  end

  # Add more helper methods to be used by all tests here...
  def add_http_authentication_header(email, password)
    request.env['HTTP_AUTHORIZATION'] = encode_credentials(email, password)
  end

  def encode_credentials(email, password)
    ActionController::HttpAuthentication::Basic.encode_credentials(email, password)
  end

  def response_json
    JSON.parse(response.body)
  end

  def setup
    DatabaseCleaner.start
  end

  def teardown
    DatabaseCleaner.clean
  end
end
