class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :description
      t.boolean :is_done, default: false
      t.references :list
      t.references :user

      t.timestamps null: false
    end
  end
end
