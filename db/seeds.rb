# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = User.create!(email: 'admin@test.com',   password: 'admin_password',   role: 'admin')
user  = User.create!(email: 'user@test.com',    password: 'user_password',    role: 'user')
tester= User.create!(email: 'tester@test.com',  password: 'tester_password',  role: 'user')
guest = User.create!(email: 'guest@test.com',   password: 'guest_password',   role: 'guest')

backend = user.lists.create!(title: 'Mysterious Backend To-Do List')
backend.tasks.create!(user: user, description: 'Create models', is_done: true)
backend.tasks.create!(user: user, description: 'Create controllers', is_done: true)
backend.tasks.create!(user: user, description: 'Create policies', is_done: true)
backend.tasks.create!(user: user, description: 'Implement CRUD actions and policies', is_done: true)
backend.tasks.create!(user: admin, description: 'Handle lists with a task by some one else', is_done: true)
backend.tasks.create!(user: user, description: 'Finalize serializers', is_done: true)
backend.tasks.create!(user: user, description: 'Implement frontend', is_done: false)

frontend = user.lists.create!(title: 'Mysterious Frontend To-Do List')
frontend.tasks.create!(user: user, description: 'Create models', is_done: false)
frontend.tasks.create!(user: user, description: 'Create routes', is_done: false)
frontend.tasks.create!(user: user, description: 'Create templates', is_done: false)

user_test = tester.lists.create!(title: 'Mysterious User Test To-Do List')
user_test.tasks.create!(user: tester, description: 'Create Lists', is_done: false)
user_test.tasks.create!(user: tester, description: 'Create Tasks', is_done: false)
user_test.tasks.create!(user: tester, description: 'Edit Lists', is_done: false)
user_test.tasks.create!(user: tester, description: 'Edit Tasks', is_done: false)
user_test.tasks.create!(user: tester, description: 'Destroy Lists', is_done: false)
user_test.tasks.create!(user: tester, description: 'Destroy Tasks', is_done: false)

