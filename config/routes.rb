Rails.application.routes.draw do
  scope :api do
    resources :tasks, only: %i(show index update create destroy)
    resources :lists, only: %i(show index update create destroy)
    resources :users, only: %i(show)
  end

  # devise_for :users
end
