class TaskSerializer < ActiveModel::Serializer
  include ApiSerializer

  attributes :id, :description, :is_done

  has_one :user, include: true
  has_one :list, include: true
end
