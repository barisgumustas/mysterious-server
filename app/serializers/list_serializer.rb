class ListSerializer < ActiveModel::Serializer
  include ApiSerializer

  attributes :id, :title, :links

  has_one :user, include: true

  def links
    {
      tasks: tasks_path(list_id: object.id)
    }
  end
end
