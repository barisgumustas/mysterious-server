module ApiSerializer
  extend ActiveSupport::Concern

  included do
    include Rails.application.routes.url_helpers

    embed :ids, embed_in_root: true
  end
end
