class UserSerializer < ActiveModel::Serializer
  include ApiSerializer

  attributes :id, :email, :role
end
