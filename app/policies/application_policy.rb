class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user   = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    (user.admin? || user.user?) && user.owns?(record)
  end

  def update?
    user.admin? || (user.user? && user.owns?(record))
  end

  def destroy?
    update?
  end
end
