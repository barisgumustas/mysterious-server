class ListPolicy < ApplicationPolicy
  def destroy?
    if user.user? && user.owns?(record)
      raise Pundit::NotAuthorizedError, invalid_tasks_explanation if tasks_by_other_users?
      return true
    else
      user.admin?
    end
  end

  private

  def invalid_tasks_explanation
    I18n.t 'policies.list_policy.has_tasks_by_other_users', count: not_owned_task_count
  end

  def tasks_by_other_users?
    not_owned_task_count > 0
  end

  def not_owned_task_count
    @not_owned_task_count ||= record.tasks.where('user_id != ?', user.id).count
  end
end
