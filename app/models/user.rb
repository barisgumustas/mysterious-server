class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :trackable, :rememberable
  # :registerable, :recoverable, :validatable

  has_many :lists
  has_many :tasks

  validates :role, inclusion: AVAILABLE_USER_ROLES, presence: true

  def admin?
    role == 'admin'
  end

  def user?
    role == 'user'
  end

  def guest?
    role == 'guest'
  end

  def owns?(record, accessor = nil)
    record.try(accessor || :user) == self
  end
end
