class Task < ActiveRecord::Base
  belongs_to :user
  belongs_to :list

  validates :list, presence: true
  validates :user, presence: true
  validates :description, presence: true
end
