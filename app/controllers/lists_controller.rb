class ListsController < ApplicationController
  before_action :assign_list, only: %i(show update destroy)

  def show
    render json: @list
  end

  def index
    authorize List

    render json: List.all
  end

  def update
    @list.assign_attributes(list_params)
    @list.save ? render(json: @list) : render_failure(@list)
  end

  def create
    list = current_user.lists.build(list_params)
    authorize list
    list.save ? render(json: list) : render_failure(list)
  end

  def destroy
    @list.destroy ? render_success_response : render_failure(@list)
  end

  protected

  def list_params
    params.fetch(:list, {}).permit(:title)
  end

  def assign_list
    @list = List.find(params[:id])

    authorize @list
  end
end
