class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :null_session

  before_action :http_authenticate

  rescue_from Pundit::NotAuthorizedError, with: :handle_policy_exceptions
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

  attr_reader :current_user

  respond_to :json

  def http_authenticate
    authenticate_with_http_basic do |email, password|
      user = User.find_by(email: email)
      @current_user = user if user.present? && user.valid_password?(password)
    end

    render_unauthorized_response if current_user.blank?
  end

  def handle_policy_exceptions(exception)
    return render_unauthorized_response([exception.to_s]) unless exception.policy.present?
    policy_name = exception.policy.class.to_s.underscore
    message     = I18n.t("#{policy_name}.#{exception.query}", scope: 'policies')
    render_unauthorized_response([message])
  end

  def render_unauthorized_response(errors = nil)
    render json: { errors: errors.blank? ? ['Unauthorized'] : errors }, status: 401
  end

  def render_not_found_response
    render json: { errors: ['Requested resource not found.'] }, status: 404
  end

  def render_failure(operation)
    render json: { errors: operation.errors.full_messages }, status: 422
  end

  def render_success_response
    render json: { }
  end
end
