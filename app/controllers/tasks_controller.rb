class TasksController < ApplicationController
  before_action :assign_task, only: %i(show update destroy)

  def show
    render json: @task
  end

  def index
    authorize Task

    tasks = params[:list_id].present? ? Task.where(list_id: params[:list_id]) : Task.all
    render json: tasks
  end

  def update
    @task.assign_attributes(task_params)
    @task.save ? render(json: @task) : render_failure(@task)
  end

  def create
    task = current_user.tasks.build(task_params)
    authorize task
    task.save ? render(json: task) : render_failure(task)
  end

  def destroy
    @task.destroy ? render_success_response : render_failure(@task)
  end

  protected

  def task_params
    params.fetch(:task, {}).permit(:description, :list_id, :is_done)
  end

  def assign_task
    @task = Task.find(params[:id])

    authorize @task
  end
end
