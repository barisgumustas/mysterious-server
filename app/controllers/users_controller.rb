class UsersController < ApplicationController
  def show
    return redirect_to_self if params[:id] == 'me'
    render json: user
  rescue ActiveRecord::RecordNotFound
    render_not_found_response
  end

  private

  def redirect_to_self
    redirect_to user_path(id: current_user.id)
  end

  def user
    @user ||= User.find(params[:id])
  end
end
